---
layout: default
title: ''
---
## Hi!

I am a software engineer living between Berlin and Barcelona and have more than 13 years of experience in full-stack development and operation of infrastructure on bare metal.
At the moment I mainly develop Golang applications close to Kubernetes and the broader cloud native ecosystem.
Furthermore, I am excited about everything evolving around Nix(OS) and enjoy contributing to Nixpkgs.

### Contact

<div class="center-left-outer">
  <div class="center-left-inner line-height-2">
    <a class="contact" href="mailto:mail@arik-grahl.de">mail@arik-grahl.de</a><br />
    <a class="contact" href="https://matrix.to/#/@arik:matrix.arik-grahl.de" target="_blank" rel="nofollow noopener">@arik:matrix.arik-grahl.de</a><br />
    <a class="contact" href="https://gitlab.com/arik.grahl" target="_blank" rel="nofollow noopener">gitlab.com/arik.grahl</a><br />
    <a class="contact" href="https://github.com/arikgrahl" target="_blank" rel="nofollow noopener">github.com/arikgrahl</a><br />
    <a class="contact" href="https://www.linkedin.com/in/arikgrahl/" target="_blank" rel="nofollow noopener">linkedin.com/in/arikgrahl</a><br />
    <a class="contact" href="https://chaos.social/@arikgrahl" target="_blank" rel="me nofollow noopener">chaos.social/@arikgrahl</a>
  </div>
</div>

#### PGP

<div class="center">
  <a href="https://openpgpkey.arik-grahl.de/.well-known/openpgpkey/arik-grahl.de/hu/dizb37aqa5h4skgu7jf1xjr4q71w4paq">
    <code class="language-plaintext">C24D 5259 8342 B27E 11D0 2678 438B 51CF 7A6E 1A03</code>
  </a>
</div>
