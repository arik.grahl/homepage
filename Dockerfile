FROM ruby:3.1.2 AS builder

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN bundle install --path vendor
RUN bundle exec jekyll build -d public

FROM nginx:1.23.1-alpine

COPY --from=builder /usr/src/app/public /usr/share/nginx/html
