document.addEventListener("DOMContentLoaded", function () {
	var html = document.querySelector('html')
	var selector = document.querySelector('.theme-selector input')
	var favicon = document.querySelector('head [rel="icon"]')
	var currentTheme = 'auto';

	function setTheme(theme) {
		currentTheme = theme;
		html.removeAttribute('class');
		html.classList.add(theme);
		document.cookie = 'theme=' + theme;

		if (theme == 'light') {
			selector.value = 0;
		} else if (theme == 'dark') {
			selector.value = 2;
		} else {
			selector.value = 1;
		}
	}

	document.cookie.split(';').forEach(function (cookie) {
		if (cookie.split('=')[0].trim() == 'theme') {
			currentTheme = cookie.split('=')[1].trim();
			return;
		}
	});

	setTheme(currentTheme);

	selector.addEventListener('input', function () {
		var theme = 'auto';
		if (this.value == '0') {
			theme = 'light';
		} else if (this.value == '2') {
			theme = 'dark';
		}
		setTheme(theme);
	});

	window.matchMedia('(prefers-color-scheme: light)').addListener(function (e) {
		if (currentTheme == 'auto') {
			setTheme(currentTheme);
		}
	});
});
